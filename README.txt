This project contains small scripts for reading and plotting ORCA/PIMC electron
density.

Executable files are:
    ORCA_eldens/generate_potential.py
                Read 3d-cube-density and write out radial density and
                corresponding electric potential

    ORCA_eldens/orb_plot.py :
                Plot radial densites and projections, from current directory's
                ORCA 3d-cube-densities.

    energy_level_diagram/level_diagram.py
                Plot hard-coded values about 4 He energies: singlet-triplet,
                coulombic correlation on/off

    PIMC_eldens/PIMC_radial_eldens.py
                Read PIMC paircorrelation and plot radial electron density.


main.py is useless, and is used for testing purposes.





Licence is MIT for now.
