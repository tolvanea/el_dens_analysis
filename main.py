import matplotlib.pyplot as plt
from testing_playground import testing_playground_2 as test

"""
This main.py is useless, and is used for testing purposes. See README.txt

Read and plot radial densities. 
"""



def main():


    #plotting.He_triplet_AOs()
    #plotting.He_ao_mo()
    #plotting.He_plus()
    #test.test_potential_integration()
    test.He_plus()

    plt.show()


if __name__ == "__main__":
    main()
