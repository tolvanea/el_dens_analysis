import numpy as np


def read_some_unclear_radial_values():
    """Read values from file generated by example in one pdf. (In atomic units.)
    """
    my_data = np.genfromtxt('datas/He-nat1s.dat', delimiter=' ')
    r = my_data[:, 0]
    psi = my_data[:, 1]
    norm_dn = (psi ** 2 * 4 * np.pi * r ** 2).sum()

    #test_some_unclear_radial_values(r, psi)
    bohr = 1

    return r/bohr, psi/norm_dn


def read_eldens3d():
    """
    Read orca file *.eldens.3d. I do not know how did I generate one.
    (In atomic units.)
    """

    with open("cc-PV6Z.eldens.3d") as f:
        for i, line in enumerate(f):
            if i == 0:
                continue
            if i == 1:
                grid_size = np.array([int(v) for v in line.split()])
            if i == 2:
                box_base_crd = np.array([float(v) for v in line.split()])
            if i == 3:
                voxel_length = np.array([float(v) for v in line.split()])
            if i == 4:
                break

    if (np.abs(grid_size - grid_size.mean()).sum() > 0.0001 or
            np.abs(box_base_crd - box_base_crd.mean()).sum() > 0.0001 or
            np.abs(voxel_length - voxel_length.mean()).sum() > 0.0001):
        assert False  # "Grid has not cubic symmetry! Implement it yourself."
    else:
        grid = grid_size[0]
        base = box_base_crd[0]
        voxel = voxel_length[0]

    flat = np.genfromtxt('cc-PV6Z.eldens.3d', delimiter='\n', skip_header=4)
    mat = flat.reshape(grid_size)

    #center3d = (grid / 2 - 1, grid / 2 - 1, grid / 2 - 1)
    #center3d = radial.center_of_mass(mat)
    #r_x3d, r_y3d = radial.radial_profile_3d(mat, (grid_size * voxel_length)[0])
    # r_x3d = np.linspace(0, grid / 2, len(r_y3d))

    class Eldens_3d():
        def __init__(self):
            #bohr = 0.52917721067
            bohr = 1
            self.mat = mat
            self.grid_size = grid
            self.box_base = base / bohr
            self.voxel_length = voxel / bohr
            self.box_size = self.grid_size * self.voxel_length

    return Eldens_3d() #r_x3d, r_y3d, (mat, grid)


def read_gaussian_3d_cube_grid_format(name="cc-PV6Z.eldens.cube", ax=None):
    """
    Read gaussian generated 3d grid format. (In atomic units.)
    :return:
    """

    if not ((".mo" in name) or (".ao" in name)):
        raise Exception(
            "Just to make sure, let it be informed that you are not reading \n"
            + "molecular orbital. It's fine to read electronic density, but\n"
            + "remember to remove the part in which matrix values are squared.")

    with open(name) as f:
        i = 0  # line number
        f.readline(); i += 1
        f.readline(); i += 1
        assert i == 2
        if True:  # There's no other way to create a block in python
            line = f.readline();  i += 1
            parts = line.split()
            assert len(parts) == 4
            num_atoms = int(parts[0])
            if np.abs(num_atoms) != 1:
                raise Exception("This script works only with one atom.")
            xc, yc, zc = [float(v) for v in parts[1:]]
            if np.abs(2*xc - yc - zc) > 0.0001:
                raise Exception(
                    "Grid has not cubic symmetry! Implement it yourself.")

        base_vector = xc

        assert i >= 3 and i <= 5
        if True:
            for j in range(3):
                line = f.readline();  i += 1
                parts = line.split()
                assert len(parts) == 4

                grid_len = int(parts[0])
                xv, yv, zv = [float(v) for v in parts[1:]]
                if j >= 1:
                    if (grid_len_old != grid_len
                            or
                            (j == 1 and (np.abs(xv_old - yv) > 0.0001))
                            or
                            (j == 2 and (np.abs(yv_old - zv) > 0.0001))):
                        raise Exception(
                                "Grid has not cubic symmetry! Implement it yourself.")

                grid_len_old = grid_len
                xv_old, yv_old, zv_old = xv, yv, zv

            if grid_len < 0:
                unit = "Angstrom"
            else:
                unit = "Bohr"

        assert i == 6
        if True:
            line = f.readline();  i += 1
            assert len(line.split()) == 5
            atom_num, charge_I_guess, x, y, z = np.array([float(v) for v in line.split()])
        assert i == 7

    #flat = np.genfromtxt('cc-PV6Z.eldens.cube', delimiter='   ', skip_header=7,
    #                     filling_values=np.float_("nan"), loose=True)
    flat = read_moronic_gaussian_format(name, grid_len**3)
    mat = flat.reshape((grid_len, grid_len, grid_len))

    #print("flat.shape", flat.shape)
    #print("flat values", flat[:100])
    #print("divisions", flat.shape[0], flat.shape[0] / grid_len,
    #      flat.shape[0] / (grid_len**2), flat.shape[0] /(grid_len**3))
    #print("mat.shape", mat.shape)

    class data_3d():
        def __init__(self):
            #bohr = 0.52917721067
            bohr = 1
            self.mat = mat
            self.grid_size = grid_len
            self.box_base = xc * bohr
            self.voxel_length = zv * bohr
            self.box_size = grid_len * zv * bohr
            self.num_atoms = num_atoms

    return data_3d()

def read_moronic_gaussian_format(name, flat_size, skip=7):
    with open(name, 'r') as f:
        for i, line in enumerate(f):
            if i == skip -1:
                break

        b, e = 0,0
        a = np.empty(flat_size, dtype=np.float_)

        # error check to start reading volume-data from right line
        for line in f: # I don't know how to fetch only one line...
            if len([float(p) for p in line.split()]) != 6:
                continue
            else:
                parts = [float(p) for p in line.split()]
                e = b + len(parts)
                a[b:e] = parts
                b = e
                break

        # read volume data
        for line in f:
            parts = [float(p) for p in line.split()]
            e = b + len(parts)
            a[b:e] = parts
            b = e
        #print("flat_size, len(a), nonzero", flat_size, len(a), len(np.nonzero(a)[0]))
        return a

