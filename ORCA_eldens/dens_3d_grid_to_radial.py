import numpy as np
from typing import Tuple

def calculate_normalized_radial_profile(data, voxel_length, grid_size, ax=None):
    """ Calculate radial 1d-density out of 3d-density. Returned density is 
    cumulative sum on sphere surface on distance r from center point. Center 
    point is chosen to be on the maximum value of gived 3d-density.
    """
    assert len(data.shape) == 3
    assert data.shape[0] == data.shape[1] == data.shape[2]
    assert np.isscalar(voxel_length)
    assert np.isscalar(grid_size)

    vl = voxel_length
    gs = grid_size
    bl = gs*vl                # box length

    # Notice normalization so that matrix value is not scaled by gridsize
    rx, ry = radial_profile_3d(data / (vl ** 3), bl)
    to_corner = rx[-1]

    q = data.shape[0] // 4

    sum_dens = data.sum()
    #print("cubic density sum: {:.6f}".format(sum_dens))

    V_s = ((4 / 3) * np.pi * to_corner ** 3)  # Volume of sphere that covers the box

    sum_dens_of_sphere = np.trapz(ry * 4 * np.pi * rx ** 2, x=rx)
    #print("sphere dens sum", sum_dens_of_sphere)
    #print("multiplicative error: ", sum_dens_of_sphere/sum_dens)

    if ax is not None:
        ax.plot(rx, ry)
        ax.plot(rx, ry * 4 * np.pi * rx ** 2)


    return rx, ry/sum_dens_of_sphere

def normalize_radial_profile(rx, ry) -> Tuple[np.ndarray, np.ndarray]:
    sum_dens_of_sphere = np.trapz(ry * 4 * np.pi * rx ** 2, x=rx)
    return rx, ry / sum_dens_of_sphere



# https://stackoverflow.com/questions/21242011/most-efficient-way-to-calculate-radial-profile
def radial_profile_3d(data, box_length, center=None):
    """
    Calculate radial profile from uniform cubic 3d-data matrix. By 'radial profile'
    is meant mean value of function on distance r from center. Used in density
    calulations.

    :param data:                Density, 3d numpy array
    :param box_length:          Length of side of a box. e.g. '7'
    :param center_in_ids:       (optional) Radial center in coordinates that
                                which has origo in box corner. e.g (4.5, 4.5, 4.5)
                                If None, center of mass is used.
    :return:    r_grid,         Grid of radial values (r)
                radialprofile   Mean of density on sphere surface which has
                                radius r
    """
    assert len(data.shape) == 3
    assert data.shape[0] == data.shape[1] == data.shape[2]
    assert np.isscalar(box_length)

    if center is None:
        center_in_ids = center_of_mass(data)
    else:
        center_in_ids = np.array(center) / box_length

    #print("max_r", max_r, gridsize, box_length, np.arange(gridsize).shape)

    # calculate radial profile
    x, y, z = np.indices(data.shape)
    xc, yc, zc = center_in_ids
    r_f = np.sqrt((x - xc) ** 2 + (y - yc) ** 2 + (z - zc) ** 2)
    r = r_f.astype(np.int)

    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())
    radialprofile = tbin / nr

    # max distance, could be gathered with len(nr) or len(tbin)
#    max_r = -1
#    for i in [0, data.shape[0]]:
#        for j in [0, data.shape[1]]:
#            for k in [0, data.shape[2]]:
#                # distances from center for corner voxels
#                r = np.sqrt(np.sum((center_in_ids - np.array((i, j, k))) ** 2))
#                max_r = max(max_r, r)
    max_r = len(nr)+1
        #print("max_r", max_r, len(tbin), "assdad", tbin[-10:], nr[-10:])
    gridsize = data.shape[0]
    # calculate grid for radial distances
    r_grid = np.linspace(0, max_r/gridsize * box_length, len(radialprofile))

    return r_grid, radialprofile


def center_of_mass(data, grid_step=None, min_max=None):
    """
    Calculate center of mass in cubic density grid. If a negative value is
    detected in array, then it is assumed to be wave function for which squaring
    is made before finding center of mass.

    :param grid_step:    (optional) ndarray, dim-3
    :param min_max:      (optional) ndarray, dim-3, max and min in tuple
                         e.g ((0,0,0), (1,1,1))
    :return:
    """
    assert not((grid_step is not None) and (min_max is not None))
    assert data.shape[0] == data.shape[1] == data.shape[2]

    gs = data.shape  # grid size

    if (min_max is None) and (grid_step is None):
        grid_step = np.array((1, 1, 1))
        min = np.array((0, 0, 0))
        max = gs
        box_length = min + gs * grid_step
    elif grid_step is  None:
        min = np.array(min_max[0])
        max = np.array(min_max[1])
        assert len(min) != 3 or len(max) != 3
        diff_min = np.abs(min[0]-min).sum()
        diff_max = np.abs(max[0]-max).sum()
        assert diff_max < 0.0001
        assert diff_min < 0.0001
        box_length = max - min
        grid_step = box_length/gs
    elif min_max is None:
        min = np.array((0, 0, 0))
        max = gs
        box_length = min + gs * grid_step

    # if negative values exists
    if (np.sum(data.ravel() < 0) > 0):
        dat = data ** 2
    else:
        dat = data

    proj_x = dat.sum(axis=(1,2))
    proj_y = dat.sum(axis=(0,2))
    proj_z = dat.sum(axis=(0,1))

    ids_x = np.arange(gs[0])
    ids_y = np.arange(gs[1])
    ids_z = np.arange(gs[2])

    center_x = ((ids_x * grid_step[0] + min[0]) * proj_x).sum() / proj_x.sum()
    center_y = ((ids_y * grid_step[1] + min[1]) * proj_y).sum() / proj_y.sum()
    center_z = ((ids_z * grid_step[2] + min[2]) * proj_z).sum() / proj_z.sum()

    cntr = np.array((center_x, center_y, center_z))

    if abs(np.sum(cntr - (max + min)/2)) > max[0] * 0.1:
        print("Center of mass: ", cntr, "assumed center:", (max + min)/2)
        print("Center of mass is not in center of box. Consider it false")

    return cntr
