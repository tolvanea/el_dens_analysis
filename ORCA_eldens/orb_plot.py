import numpy as np
import matplotlib.pyplot as plt
from ORCA_eldens import read_density_formats as formats, \
    dens_3d_grid_to_radial as radial
from scipy.interpolate import UnivariateSpline
import os

"""
Plot general information about 3d-orbitals in current directory. Orbitals are 
gaussian *.cube format, that can be generated with orca_plot.

Usage
    cd the_dircetory_that_contains_.cube_files
    python3 -m orb_plot
"""

MARKERS = [".", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8", "s",
               "p", "P", "*"]  # all plotting dot markers

# plot_radial_densities
def main():
    """ Plot radial densities

    Searches gaussian cube format files from current folder and plots the
    radial densities.
    """

    files = []
    for filename in sorted(os.listdir()):
        if filename[-5:] == ".cube":
            files.append(filename)

    #ax1, ax2 = figi2()
    if len(files) <= 4:
        axs = figi_1_4()
    else:
        axs = figi_1_8()

    fig = axs[0]
    ax1= axs[1]
    plotcount = 0

    for filename in files:
        wf_3d = formats.read_gaussian_3d_cube_grid_format(filename)
        rx, ry = radial.calculate_normalized_radial_profile(wf_3d.mat ** 2,
                                                            wf_3d.voxel_length,
                                                            wf_3d.grid_size)
        orb, NO, sp= get_orb_type_and_NO(filename)
        sp, spin, orbital = further_modifications(sp, orb)
        label = "{}{}{}".format(orbital, MO_to_shellnum(NO), spin)

        plotcount += 1
        plot_it(axs, ax1, wf_3d, rx, ry, plotcount, label, NO)

        print_root(rx, ry, orb, NO, sp)

    ax1.legend()
    fig.savefig("orbitals.pdf")
    plt.show()


def get_orb_type_and_NO(name):
    """Extract from filename [orbital type], [the number] and [spin].

    Preconditions:
        string contains only once ".ao" or ".mo"
        NO <= 9"""

    if ".ao" in name:
        idx = name.find(".ao")
        return "ao", int(name[idx + 3]), ""
    elif ".mo" in name:
        idx = name.find(".mo")
        return "mo", int(name[idx + 3]), name[idx + 4]
    else:
        assert False

def MO_to_shellnum(MO):
    if MO == 0:
        return "1s"
    elif MO == 1:
        return "2s"
    elif MO >= 2 and MO <= 4:
        return "2p_" + "xyz"[MO-2]
    elif MO == 5:
        return "3s"
    elif MO >= 6 and MO <= 8:
        return "3p_" +"xyz"[MO-6]
    elif MO >= 9 and MO <= 14:
        orbs = ["z^2", "xz", "yz", "xy", "x^2-y^2"]
        return "3p_" + orbs[MO - 9]

def further_modifications(sp, orb):
    if sp == "" or sp == "a":
        sp = "a"
        spin = ""
    else:
        spin = ", spin: {}".format(sp)
    if orb == "mo":
        orbital = ""
    else:
        orbital = "{}: ".format(orb)

    return sp, spin, orbital

def plot_it(axs, ax1, wf_3d, rx, ry, plotcount, label, NO):
    ax1.plot(rx, ry,
             label=label,
             marker=MARKERS[NO])
    ax1.set_title("Radial density")
    ax1.set_ylim((-0.01, 0.1))
    ax1.set_xlim((-0.01, 6))
    ax1.set_xlabel("r ($a_0$)")


    if plotcount <= 8:
        a = wf_3d.box_size / 2
        lims = [-a, a, -a, a]
        axs[plotcount + 1].imshow(np.sum(wf_3d.mat, 0),
                                  extent=lims,
                                  cmap='hot', interpolation='nearest')
        axs[plotcount + 1].set_title(label)

def print_root(rx, ry, orb, NO, spin):
    spl = UnivariateSpline(rx, ry, k=4, s=0)
    der = spl.derivative(1)
    der2 = spl.derivative(2)
    roots = der.roots()
    print("{} ({}:{}, s:{})".format(MO_to_shellnum(NO), orb, NO, spin))
    for r in roots:
        if der2(r) > 0.01 and spl(r) < 0.01:
            print(
                " -- minima:{}, der2:{}".format(r, der2(r)))

    return spl


def figi4(seed=None):
    if seed == None:
        seed = np.random.randint(100)
    fig = plt.figure(figsize=(9, 7), num="figi{}".format(seed))
    ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=1, rowspan=1)
    ax2 = plt.subplot2grid((2, 2), (0, 1), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((2, 2), (1, 0), colspan=1, rowspan=1)
    ax4 = plt.subplot2grid((2, 2), (1, 1), colspan=1, rowspan=1)
    #fig.suptitle("Results")

    return fig, (ax1, ax2, ax3, ax4)

def figi2(seed=None):
    if seed == None:
        seed = np.random.randint(100)
    fig = plt.figure(figsize=(9, 4), num="figi{}".format(seed))
    ax1 = plt.subplot2grid((1, 2), (0, 0), colspan=1, rowspan=1)
    ax2 = plt.subplot2grid((1, 2), (0, 1), colspan=1, rowspan=1)
    #fig.suptitle("Results")

    return ax1, ax2

def figi_1_4(seed=None):
    if seed == None:
        seed = np.random.randint(100)
    fig = plt.figure(figsize=(12, 7), num="figi{}".format(seed))
    ax1 = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((2, 4), (0, 2), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((2, 4), (0, 3), colspan=1, rowspan=1)
    ax4 = plt.subplot2grid((2, 4), (1, 2), colspan=1, rowspan=1)
    ax5 = plt.subplot2grid((2, 4), (1, 3), colspan=1, rowspan=1)
    #fig.suptitle("Results")

    return (fig, ax1, ax2, ax3, ax4, ax5)

def figi_1_8(seed=None):
    if seed == None:
        seed = np.random.randint(100)
    fig = plt.figure(figsize=(12, 9), num="figi{}".format(seed))
    ax1 = plt.subplot2grid((3, 4), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((3, 4), (0, 2), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((3, 4), (0, 3), colspan=1, rowspan=1)
    ax4 = plt.subplot2grid((3, 4), (1, 2), colspan=1, rowspan=1)
    ax5 = plt.subplot2grid((3, 4), (1, 3), colspan=1, rowspan=1)
    ax6 = plt.subplot2grid((3, 4), (2, 0), colspan=1, rowspan=1)
    ax7 = plt.subplot2grid((3, 4), (2, 1), colspan=1, rowspan=1)
    ax8 = plt.subplot2grid((3, 4), (2, 2), colspan=1, rowspan=1)
    ax9 = plt.subplot2grid((3, 4), (2, 3), colspan=1, rowspan=1)
    #fig.suptitle("Results")

    return (fig, ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9)


"""
H 2s analytical nodes:  [1.99999994]
H 3s analytical nodes:  [1.90192375 7.09807623]
He+ 2s analytical nodes:  [0.99999983]
He+ 3s analytical nodes:  [0.95096108 3.54903815]
H analytical energy, n:1, E:-0.49972237645752354
H analytical energy, n:2, E:-0.12493059411438089
H analytical energy, n:3, E:-0.05552470849528039
He+ analytical energy, n:1, E:-1.9988895058300942
He+ analytical energy, n:2, E:-0.49972237645752354
He+ analytical energy, n:3, E:-0.22209883398112157
"""

if __name__ == "__main__":
    main()
