import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy import integrate
import sys

from ORCA_eldens import read_density_formats as formats, \
    dens_3d_grid_to_radial as radial

help_text = """
Reads gaussian 3d molecular orbital file and generates radial density and radial
potential to different files.

Note: Use atomic units!

Usage:
    python3 -m generate_potential cc-PV6Z.mo0a.cube
"""

def main(name):
    """
    Read in ORCA 3d-cube electron density and write out radial form of it.
    """
    write_radial_potential(name)

def write_radial_potential(name):
    """
    Read 3d orbital grid and output radial electron density in file.

    :param name: e.g. "cc-PV6Z.mo1a.cube"
    :return:
    """

    if name[-5:] != ".cube":
        raise Exception("Grid file must be gaussian cube type '*.cube'")
    if ".mo" not in name:
        raise Exception("Implemented only for molecular orbitals. If you want from density, modify this function a bit.")


    wf_3d = formats.read_gaussian_3d_cube_grid_format(name)
    # if reading density, remove '**2' :
    rx, ry = radial.calculate_normalized_radial_profile(wf_3d.mat ** 2,
                                                        wf_3d.voxel_length,
                                                        wf_3d.grid_size)
    U = radial_potential(rx, ry)
    with open(name[:-5] + ".radial_potential", "w") as f:
        for i in range(len(U)):
            f.write("{} {}\n".format(rx[i], U[i]))
    with open(name[:-5] + ".radial_density", "w") as f:
        for i in range(len(ry)):
            f.write("{} {}\n".format(rx[i], ry[i]))


def radial_potential(r, y):
    """
    Converts radial electron density to radial potential.
    :param r:  radial distanses, 1d-grid, (units probably in bohr, I can't now remember)
    :param y:  electron density, 1d-grid (automatically normalized in process)
    :return:
    """
    #rho = UnivariateSpline(r, y, s=0)  # spline of density in distance r
    rho_4_pi_r2_unnorm = UnivariateSpline(r, y * 4*np.pi*r**2, s=0)  # sum on of sphere surface

    total_dens = rho_4_pi_r2_unnorm.integral(0, r[-1])
    #print("Unnormalized unit volume: ", total_dens, ". (It should be 1.) Normalizing it anyway.")
    rho_4_pi_r2 = UnivariateSpline(r, y/total_dens * 4*np.pi*r**2, s=0)  # fix

    def eldens_sum(r2) -> float:
        """The sum of density inside sphere with radius r2"""
        return rho_4_pi_r2.integral(0, r2)

    def force(r2):
        # dielectric_constant = 1, electron_charge = 1
        return eldens_sum(r2) / r2**2  # * dielectric_constant * electron_charge^2

    def potential(r):
        U = np.empty((len(r)), dtype=np.float_)
        for i, r1 in enumerate(r):
            U[i] = - (- integrate.quad(force, r1, r[-1])[0]) + 1/r[-1]
        return U

    U = potential(r)

    return U


# Ignore following, it's testing function
def radial_potential_trapz(r, y):
    """
    This function is just copy of 'radial_potential', with different integrator.
    This uses primitive numerical trapedzoidal integration.

    :param r:  radial electron density grid
    :param y:  corresponding electron density (normalized to 1 in unit volume)
    :return:
    """

    def eldens_sum_trapz(r_idx2) -> float:
        """The sum of density inside sphere with radius r2"""
        # the part inside given sphere radius
        y_in = y[r_idx2::-1]
        r_in = r[r_idx2::-1]
        return np.trapz(y_in * 4*np.pi*r_in**2, r_in)

    def force_trapz(r_idx2, r2):
        """Calculate force to electron near electron density"""
        # dielectric_constant = 1, electron_charge = 1
        return eldens_sum_trapz(r_idx2) / r2**2  # * dielectric_constant * electron_charge^2

    def potential_trapz(r):
        U = np.empty((len(r)), dtype=np.float_)
        F = np.empty((len(r)), dtype=np.float_)

        # Calculate force for all radiai
        for i, r1 in enumerate(r):
            F[i] = force_trapz(i, r1)

        for i, r1 in enumerate(r):
            U[i] = - np.trapz(F[i:], x=r[i:]) + 1/r[-1]
        return U

    U = potential_trapz(r)
    return U

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(help_text)
        raise ValueError("Missing argument: Give name of molecular orbital 3d grid.")
    main(sys.argv[1])
