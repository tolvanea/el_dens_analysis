import numpy as np
import matplotlib.pyplot as plt

def ugly_main():
    fig = plt.figure(figsize=(5, 4), num="tasoookaaviooo_{}".format(np.random.randint(100)))
    ax = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)

    E = return_E()

    for i in range(4):
        n = len(E)  # =3 Number of systems (parallel levels)
        #if n != 3:
        #    raise Exception("Sorry n==3 this is hardcoded!")
        for j in range(n):
            X = gen_parallel_line_parts(j, n)
            plot_levels(E, j, i, n, X, ax)
            plot_level_connections(E, j, i, X, ax)
            write_text_in_plot(E, j, i, n, X, ax)

    figure_stuff(fig, ax)

def return_E():
    E = [[(-2.8618,  0.0019,  "He $^1S$ PIMC"),  # (--)
          (-0.0,     0.0,     "He $^3S$ PIMC"),  # (X-)
          (-2.90357, 0.00028, "He $^1S$ PIMC"),  # (-C)
          (-2.1290,  0.0011,  "He $^3S$ PIMC")], # (XC)

         [(-2.86167, 0.0, "He $^1S$ ORCA"),  # (--)
          (-2.09427, 0.0, "He $^3S$ ORCA"),  # (X-)
          (-2.90343, 0.0, "He $^1S$ ORCA"),  # (-C)
          (-0.0,     0.0, "He $^3S$ ORCA")], # (XC)

         [(-2.8616729, 0.0, "He $^1S$ ref."),  # (--)
          (-2.174250,  0.0, "He $^3S$ ref."),  # (X-)
          (-2.903724,  0.0, "He $^1S$ ref."),  # (-C)
          (-2.17522,   0.0, "He $^3S$ ref.")]  # (XC)
         ]
    return E


def plot_levels(E, j, i, n, X, ax):
    if np.abs(E[j][i][0]) < 0.001:
        return
    val = E[j][i][0]  # energy
    err = E[j][i][1]
    horizontal_dims = X[2 * i: 2 * i + 2]
    color = ["C0", "C1", "C2"][j]
    twice = lambda x: [x, x]

    # darn line label
    if 2 * i + j < n:
        label = E[j][i][2][-4:]
        ax.errorbar(horizontal_dims, twice(val), twice(err), color=color,
                    label=label)
    else:
        ax.errorbar(horizontal_dims, twice(val), twice(err), color=color)

def gen_parallel_line_parts(j, n):
    # level lines are divided in n parts so they can bee seen if they have same value
    x = [0, 3, 5, 8, 5, 8, 10, 13]
    X = []
    for I in range(len(x) // 2):
        span = x[2 * I + 1] - x[2 * I]
        shift = span / n
        begin = x[2 * I] + j * shift
        X.extend([begin, begin + shift*0.8])
    return X

def plot_level_connections(E, j, i, X, ax):
    C = [0, 1, 0, 2, 1, 3, 2, 3]

    # if either end does not exist
    if np.abs(E[j][C[2 * i]][0]) < 0.001 or np.abs(
            E[j][C[2 * i + 1]][0]) < 0.001:
        return

    connection = [E[j][C[2 * i]][0], E[j][C[2 * i + 1]][0]]
    # err =        [E[c[2*i + j]][1], E[c[2*i + j + 1]][1]]
    horizontal_dims = [X[2 * C[2 * i] + 1],
                       X[2 * C[2 * i + 1]]]
    color = ["C0", "C1", "C2"][j]
    style = ":"
    ax.plot(horizontal_dims, connection, color=color, linestyle=style)


def write_text_in_plot(E, j, i, n, X, ax):
    energy = E[j][i][0]
    if np.abs(energy) < 0.001:
        return
    x_crd = X[2*i] - (X[2*i+1] - X[2*i])/0.8 * j
    #y_crd = E[2][i][0]

    # Magical fidling so that text goes above or below line nicely
    Es = np.array([E[J][i][0] for J in range(n)])  # energies
    nonzero = np.abs(Es) > 0.0001
    sort = np.argsort(Es[nonzero])  # non-zero energies ordered
    #print("str, that shiut" , sort, [[E[J][i][0]] for J in range(n)])
    idx_in_nonzerolist = nonzero[:j + 1].sum() - 1
    ord = sort[idx_in_nonzerolist]  # order index of this current label
    row_sep = 0.05

    (y_level, grad_shift) = label_above_or_below_line(E, i, nonzero.sum(), row_sep)

    shift = (ord) * (1*row_sep) + grad_shift

    label = E[j][i][2][-4:]
    #rnd = np.random.rand()
    #ax.plot([x_crd+rnd, x_crd+rnd], [energy, shift + energy])

    # row: {name pimc/ORCA} {energy value}
    err = E[j][i][1]  # error disabled due to disalignment of text
    if True: #err < 0.000001:  # no error (ORCA)
        text = "{} ${:.4f}$".format(label, energy)
    else: # errorbars in pimc
        text = "{} ${:.4f}({})$".format(label, energy, int(err*10**4))
    ax.text(x_crd + 3, y_level + shift, text,  # \\;E_h
            horizontalalignment='right',
            verticalalignment='top',
            fontsize=9,
            # bbox=dict(boxstyle="round",
            #          ec=(1., 1.0, 1.0),
            #          fc=(1., 1.0, 1.0),
            #          )
            )

    if j == 2: # or (np.abs(E[0][i][0]) < 0.001):
        correlation_label__eg_XC(E, i, energy, x_crd, y_level, ax)

def label_above_or_below_line(E, i, n, row_sep):
    half_point = -2.5
    if E[2][i][0] > half_point:
        highest = -100
        for J in range(n):
            if not np.abs(E[J][i][0]) < 0.001:
                highest = max(highest, E[J][i][0])
        return (highest, n * 1*row_sep)
    else:
        lowest = 100
        for J in range(n):
            if not np.abs(E[J][i][0]) < 0.001:
                lowest = min(lowest, E[J][i][0])
        return (lowest, -0.15)

def correlation_label__eg_XC(E, i, energy, x_crd, y_crd, ax):
    correlation_label = ["{}, [{}]".format(E[0][i][2][-10:-5], xc)
                                            for xc in ["––", "X–", "–C", "XC"]]
    shift = 0.18
    if energy > -2.5:
        shift *= -1
    ax.text(x_crd + 1.5, y_crd + shift, correlation_label[i],
            horizontalalignment='center',
            verticalalignment='center',
            fontsize=12,
            bbox=dict(boxstyle="round",
                      ec=(0.9, 0.9, 0.9),
                      fc=(1., 1.0, 1.0),
                      )
            )


def figure_stuff(fig, ax):
    fig.gca().axes.get_xaxis().set_visible(False)
    # ax.set_xticklabels([])
    ax.legend(loc=2)
    ax.set_ylabel("E $(E_h)$")
    ax.set_ylim((-3.13, -1.9))
    fig.tight_layout()

    fig.savefig("E_levels.pdf")
    plt.show()


if __name__ == "__main__":
    ugly_main()