import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(5, 4), num="tasokaavio_{}".format(np.random.randint(100)))
ax = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)

E = [(-2.8749, 0.0014, "He$^1$ PIMC"),  #  (--)
     #(-2.86167, 0.0, "He$^1$ ORCA"),
     (-2.8616729, 0.0, "He$^1$ ref."),

     (-0.0, 0.0, "He$^3$ PIMC"),  #  (X-)
     #(-2.09427, 0.0, "He$^3$ ORCA"),
     (-2.174250, 0.0, "He$^3$ ref."),

     (-2.90357, 0.00028, "He$^1$ PIMC"),  #  (-C)
     #(-2.90343, 0.0, "He$^1$ ORCA"),
     (-2.903724, 0.0, "He$^1$ ref."),

     (-2.1285, 0.0013, "He$^3$ PIMC"),  #  (XC)
     #(-0.0, 0.0, "He$^3$ ORCA")]
     (-2.17146, 0.0, "He$^3$ ref.")]

correlation_label = ["––", "X–", "–C", "XC"]

x = [0,3,5,8,5,8,10,13]
c = [0,2,0,4,2,6,4,6]   # connections for pimc (ORCA idx is +1)
twice = lambda x: [x, x]
# plot levels
for i in range(4):
    # plot levels
    for j in range(2):
        if np.abs(E[2*i + j][0]) < 0.001:
            continue
        val = E[2*i + j][0]  # energy
        err = E[2*i + j][1]
        horizontal_dims = x[2*i : 2*i+2]
        color = ["C0", "C1"][j]

        # darn line label
        if 2*i + j < 2:
            label = E[2 * i + j][2][-4:]
            ax.errorbar(horizontal_dims, twice(val), twice(err), color=color, label = label)
        else:
            ax.errorbar(horizontal_dims, twice(val), twice(err), color=color)

    # plot level connections
    for j in range(2):
        if np.abs(E[c[2 * i] + j][0]) < 0.001 or np.abs(E[c[2 * i + 1] + j][0]) < 0.001:
            continue
        connection = [E[c[2 * i] + j][0], E[c[2 * i + 1] + j][0]]
        # err =        [E[c[2*i + j]][1], E[c[2*i + j + 1]][1]]
        horizontal_dims = [x[c[2 * i] + 1], x[c[2 * i + 1]]]
        color = ["C0", "C1"][j]
        style = ":"
        ax.plot(horizontal_dims, connection, color=color, linestyle=style)

    # write text in plot
    for j in range(2):
        energy = E[2*i + j][0]
        if np.abs(energy) < 0.001:
            continue
        x_crd = x[2*i]
        y_crd = E[2*i + j][0]

        # Magical fidling so that text goes above or below line nicely
        vertical_order = (E[2 * i + 0][0] < E[2 * i + 1][0])
        vert_displasement_factor = (j - 0.5) * 0.07
        if not vertical_order:
            vert_displasement_factor *= -1
        row_space = 0.04
        shift = vert_displasement_factor + row_space * (int(vert_displasement_factor > 0))

        label = E[2*i + j][2]
        rnd = np.random.rand()
        #ax.plot([x_crd+rnd, x_crd+rnd], [energy, shift + energy])

        # first row name pimc/ORCA
        ax.text(x_crd+1.5, y_crd + shift, label,
                horizontalalignment='center',
                verticalalignment='center',
                fontsize=10,
                #bbox=dict(boxstyle="round",
                #          ec=(1., 1.0, 1.0),
                #          fc=(1., 1.0, 1.0),
                #          )
                )
        # second row: energy value
        ax.text(x_crd+1.5, y_crd + shift - row_space, "${:.4f}\\;E_H$".format(energy),
                horizontalalignment='center',
                verticalalignment='center',
                fontsize=9,
                # bbox=dict(boxstyle="round",
                #          ec=(1., 1.0, 1.0),
                #          fc=(1., 1.0, 1.0),
                #          )
                )

        # correlation label e.g.'XC'
        if j == 0 or (np.abs(E[2*i][0]) < 0.001):
            pos = 0.18
            if energy > -2.5:
                pos *= -1
            ax.text(x_crd + 1.5, y_crd + pos, correlation_label[i],
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontsize=14,
                    bbox=dict(boxstyle="round",
                              ec=(0.9, 0.9, 0.9),
                              fc=(1., 1.0, 1.0),
                              )
                    )

fig.gca().axes.get_xaxis().set_visible(False)
#ax.set_xticklabels([])
ax.legend(loc=2)
ax.set_ylabel("E $(E_h)$")
ax.set_ylim((-3, -1.98))
fig.tight_layout()



plt.savefig("E_levels.pdf")
plt.show()
