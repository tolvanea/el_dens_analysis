import numpy as np
import matplotlib.pyplot as plt
from ORCA_eldens import read_density_formats as formats, \
    generate_potential as potential, dens_3d_grid_to_radial as radial, orb_plot
from testing_playground import testing_playground_3 as test3


def main():
    test_potential_integration()

    plt.show()



def He_ao_mo():
    axs = orb_plot.figi4()

    #ax1, ax2, ax3, ax4= axs1[0], axs1[1], axs1[2], axs1[3]

    markers = [".", ",", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8", "s",
               "p", "P", "*"]

    for NO in range(3):  # orbital number
        if NO <= 3:
            a = axs[NO]
        else:
            a = axs[3]

        for spin in ["a"]:  # "b"
            for orb in ["ao", "mo"]:  # orbital "mo"
                spin_label = spin if orb=="mo" else ""  # show "a"/"b" with MO:s
                eg_ao = formats.read_gaussian_3d_cube_grid_format(
                    "datas/He_ao_mo/cc-PV6Z.{}{}{}.cube".format(orb, NO, spin_label))
                rx_ga_ao, ry_ga_ao = radial.calculate_normalized_radial_profile(
                    eg_ao.mat**2, eg_ao.voxel_length, eg_ao.grid_size)
                a.plot(rx_ga_ao, ry_ga_ao, label="{}:{}, spin:{}".format(orb, NO, spin),
                       marker=markers[NO])

        a.legend()


def He_plus():
    axs = orb_plot.figi4()

    def node_of_2s(axs):
        """ Caluclulate analytical values for He+ ion, so it can be compared to
        ORCA simultion.
        """

        test3.nodes(axs[3])
        test3.energies()

    node_of_2s(axs)

    #ax1, ax2, ax3, ax4= axs1[0], axs1[1], axs1[2], axs1[3]

    markers = [".", ",", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8", "s",
               "p", "P", "*"]


    for NO in range(9):  # orbital number
        #if NO <= -1:
        #    a = axs[NO]
        #else:
        #    a = axs[3]

        for spin in ["a"]:
            for orb in ["ao", "mo"]:  # orbital
                spin_label = spin if orb=="mo" else ""  # show "a"/"b" with MO:s
                a = axs[0] if orb=="ao" else axs[1]
                dens = formats.read_gaussian_3d_cube_grid_format(
                    "datas/He+_aos/cc-PV6Z.{}{}{}.cube".format(orb, NO, spin_label))
                rx, ry = radial.calculate_normalized_radial_profile(
                    dens.mat**2, dens.voxel_length, dens.grid_size)

                spl = orb_plot.print_root(rx, ry, orb, NO, spin)

                x_tight = np.linspace(0, rx[-1], 1000)

                a.plot(x_tight, spl(x_tight), label="{}:{}, spin:{}".format(orb, NO, spin),
                       marker=markers[NO])
                a.legend()


def test_potential_integration():
    fig1 = plt.figure(figsize=(5, 4), num="figi{}".format(123))
    ax1 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)
    fig2 = plt.figure(figsize=(5, 4), num="figi{}".format(1234))
    ax2 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)
    #ax1, ax2, ax3, ax4 = figi4()

    # wave functions
#    wf_mo0 = formats.read_gaussian_3d_cube_grid_format("datas/He+_aos/cc-PV6Z.mo0a.cube")
#    wf_mo1 = formats.read_gaussian_3d_cube_grid_format("datas/He+_aos/cc-PV6Z.mo1a.cube")
    wf_mo0 = formats.read_gaussian_3d_cube_grid_format("datas/He3_for_pimc/cc-PV6Z.mo0a.cube")
    wf_mo1 = formats.read_gaussian_3d_cube_grid_format("datas/He3_for_pimc/cc-PV6Z.mo1a.cube")

    rx_mo0, ry_mo0 = radial.calculate_normalized_radial_profile(
        wf_mo0.mat**2, wf_mo0.voxel_length, wf_mo0.grid_size)
    rx_mo1, ry_mo1 = radial.calculate_normalized_radial_profile(
        wf_mo1.mat**2, wf_mo1.voxel_length, wf_mo1.grid_size)



    U_mo0 = potential.radial_potential(rx_mo0, ry_mo0)
    U_mo1 = potential.radial_potential(rx_mo1, ry_mo1)
    ax1.plot(rx_mo0, U_mo0, label="1s")
    ax1.plot(rx_mo1, U_mo1, label="2s")
    ax1.plot(rx_mo0, 1/rx_mo0, label="pistevaraus")
    ax1.legend()
    ax2.set_title("Radial potential")
    print("U_mo0[0]", U_mo0[0])

    ax2.plot(rx_mo0, ry_mo0, label="mo0")
    ax2.plot(rx_mo1, ry_mo1, label="mo1")

    ax2.plot(rx_mo0, ry_mo0 * 4*np.pi*rx_mo0**2, label="mo0")
    ax2.plot(rx_mo1, ry_mo1 * 4*np.pi*rx_mo0**2, label="mo1")
    ax2.set_title("Radial density * 4*pi*r^2")
    ax2.legend()

    #def potential_f(r):
    #    U = np.empty((len(r)), dtype=np.float_)
    #    for i, r1 in enumerate(r):
    #        #U[i] = - (- integrate.quad(force, r1, r[-1])[0])
    #        U[i] = - (- integrate.quad(lambda r0: 1/r0**2, r1, r[-1])[0])
    #    return U
#
#
    #U = potential_f(r)

    #U_mo0_trapz = potential.radial_potential_trapz(rx_mo0, ry_mo0)
    #U_mo1_trapz = potential.radial_potential_trapz(rx_mo1, ry_mo1)
    #ax1.plot(rx_mo0, U_mo0_trapz, label="mo0 trapz")
    #ax1.plot(rx_mo1, U_mo1_trapz, label="mo1 trapz")
    #ax3.plot(rx_mo0, 1/rx_mo0, label="coulomb U")
    #ax3.set_title("Potential")


    ax1.legend()

def idk_something(axs1):
    """Compare three different methods to gain density"""

    # Read example in one pdf-file, These are alledgedly natural orbitals, but I
    # do not believe that.
    rx_u, ry_u = formats.read_some_unclear_radial_values()
    rx_u, ry_u = radial.normalize_radial_profile(rx_u, ry_u ** 2)

    # Read some *.eldens.3d format. I am not sure how that is generated with orca.
    ed = formats.read_eldens3d()
    rx_ed, ry_ed = radial.calculate_normalized_radial_profile(ed.mat,
                                                              ed.voxel_length,
                                                              ed.grid_size)  # axs1[1]
    # gaussian cube
    eg = formats.read_gaussian_3d_cube_grid_format()

    rx_ga, ry_ga = radial.calculate_normalized_radial_profile(eg.mat,
                                                              eg.voxel_length,
                                                              eg.grid_size)  # axs1[1]

    compare_the_two(rx_u, ry_u, "nat orb",
                    rx_ed, ry_ed, "e dens",
                    axs1[1], "Normalized, nat - eldens")

    compare_the_two(rx_ga, ry_ga, "gaussian",
                    rx_ed, ry_ed, "e dens",
                    axs1[2], "Normalized gaussian - eldens")

    compare_the_two(rx_ga, ry_ga, "gaussian",
                    rx_u, ry_u, "nat",
                    axs1[3], "Normalized gaussian - nat")

def He_triplet_AOs():
    axs1 = orb_plot.figi4()

    ax, ax1, ax2, ax3 = axs1[0], axs1[1], axs1[2], axs1[3]

    markers = [".", ",", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8", "s", "p", "P", "*"]

    if False: # plot third orbital densities projection x y z
        ao = 1
        eg_ao = formats.read_gaussian_3d_cube_grid_format("datas/He_triplet_aos/cc-PV6Z.mo{}a.cube".format(ao))
        rx_ga_ao, ry_ga_ao = radial.calculate_normalized_radial_profile(
            eg_ao.mat ** 2, eg_ao.voxel_length, eg_ao.grid_size)
        ax.plot(rx_ga_ao, ry_ga_ao, label="ao{}".format(ao), marker=markers[ao])

        ax1.imshow(np.sum(eg_ao.mat, 0), cmap='hot', interpolation='nearest')
        ax2.imshow(np.sum(eg_ao.mat, 1), cmap='hot', interpolation='nearest')
        ax3.imshow(np.sum(eg_ao.mat, 2), cmap='hot', interpolation='nearest')

        ax.legend()
        return

    axs = [ax, ax1, ax2, ax3]
    for ao in range(5):
        if ao <= 3:
            a = axs[ao]
        else:
            a = axs[3]

        for sp, lb in [(-1, "a"), (1, "b")]:
            eg_ao = formats.read_gaussian_3d_cube_grid_format("datas/He_triplet_aos/cc-PV6Z.mo{}{}.cube".format(ao, lb))
            rx_ga_ao, ry_ga_ao = radial.calculate_normalized_radial_profile(
                eg_ao.mat, eg_ao.voxel_length, eg_ao.grid_size)
            a.plot(rx_ga_ao, ry_ga_ao, label="ao{}, sp{}".format(ao, sp), marker=markers[ao])

            a.legend()


def compare_the_two(rx1, ry1, label1,
                    rx2, ry2, label2,
                    ax, title=""):
    ax.plot(rx1, ry1, label=label1)
    #ax.plot(rx1, ry1 ** 2, label=label1+" squared")
    ax.plot(rx2, ry2, label=label2)
    ax.set_title(title)
    ax.legend()


if __name__ == "__main__":
    main()