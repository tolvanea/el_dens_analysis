import numpy as np
import matplotlib.pyplot as plt
from ORCA_eldens import read_density_formats as formats, \
    dens_3d_grid_to_radial as radial, orb_plot
from scipy.interpolate import UnivariateSpline
import os


def main():
    # print analytical nodes and energies for H and He+
    #nodes()
    #energies()

    # Compare ORCA to analytical
    compare_to_my_own()



def nodes(axs=None):
    """Analytical nodes for {H and He+} : {2s and 3s}"""

    # https://quantummechanics.ucsd.edu/ph130a/130_notes/node233.html

    a0 = 1
    for lb, Z in (("H", 1), ("He+", 2)):
        r = np.linspace(0,10,100)
        # radial function of n=2, l=0

        F = 1 / np.sqrt(2 * np.pi)  # constant for phi
        P = 1 / np.sqrt(2)      # constant for theta

        R_10 = 2* (Z/a0)**(3/2) * np.exp(-Z*r/a0)
        R_20 = 2 * (Z / (2 * a0)) ** (3 / 2) * (1 - (Z * r) / (2 * a0)) * np.exp(
            -Z * r / (2 * a0))

        # radial function of n=2, l=0
        part1 = 2 * (Z / (3 * a0)) ** (3 / 2)
        part2 = (1 - (2 * Z * r) / (3 * a0) + (2 * (Z * r) ** 2) / (
                    27 * a0 ** 2))
        part3 = np.exp(-Z * r / (3 * a0))
        R_30 = part1 * part2 * part3

        rho_10 = (F * P * R_10)**2
        rho_20 = (F * P * R_20)**2
        rho_30 = (F * P * R_30)**2

        spl20 = UnivariateSpline(r, rho_20, s=0)
        spl30 = UnivariateSpline(r, rho_30, s=0)
        print("{} 2s analytical nodes: ".format(lb), spl20.roots())  # >> [0.99999986]
        print("{} 3s analytical nodes: ".format(lb), spl30.roots())  # >> [0.95096181 3.54903814]

        if axs is not None:
            axs[0].plot(r, rho_10, label="{} 1s analytical ".format(lb))
            axs[1].plot(r, rho_20, label="{} 2s analytical ".format(lb))
            axs[2].plot(r, rho_30, label="{} 3s analytical ".format(lb))
            axs[0].legend()
            axs[1].legend()
            axs[2].legend()

def energies():
    """Analytical energies for {H and He+} : {1s, 2s and 3s}"""
    a0 = 1
    m_N = 1800
    m_e = 1
    micro = (m_N*m_e)/(m_N + m_e)  # reduced mass
    alpha = 1 / 137.036
    c = 1/alpha
    for lb, Z in (("H", 1), ("He+", 2)):
        for n in [1, 2, 3]:
            nom = micro * c**2 * Z**2 * alpha**2
            den = 2 * n**2
            E_n = - nom / den
            print("{} analytical energy, n:{}, E:{}".format(lb, n, E_n))


def compare_to_my_own():
    """Compare ORCA H He+ to analytical"""
    fig, axs = orb_plot.figi4()
    #ax1 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)
    def ORCA_part(axs):
        for atom_label, dir in (("H", "H_mos"), ("He+", "He+_mos")):
            files = []
            for filename in sorted(os.listdir(dir)):
                if filename[-5:] == ".cube":
                    files.append(dir + "/" + filename)


            plotcount = 0

            for i, filename in enumerate(files):
                wf_3d = formats.read_gaussian_3d_cube_grid_format(filename)
                rx, ry = radial.calculate_normalized_radial_profile(wf_3d.mat ** 2,
                                                                    wf_3d.voxel_length,
                                                                    wf_3d.grid_size)
                orb, NO, sp = orb_plot.get_orb_type_and_NO(filename)
                sp, spin, orbital = orb_plot.further_modifications(sp, orb)
                label = "{}: {}{}{}".format(atom_label, orbital, orb_plot.MO_to_shellnum(NO), spin)

                plotcount += 1
                plot_it_yeah(axs[i], wf_3d, rx, ry, plotcount, label, NO)

                orb_plot.print_root(rx, ry, orb, NO, sp)

    ORCA_part(axs)
    nodes(axs=axs)
    energies()
    fig.suptitle("Radial electron density")
    axs[0].legend()
    axs[1].legend()
    axs[2].legend()
    axs[0].set_ylabel("$\\rho$")
    axs[1].set_ylabel("$\\rho$")
    axs[2].set_ylabel("$\\rho$")
    axs[0].set_title("1s")
    axs[1].set_title("2s")
    axs[2].set_title("3s")
    axs[2].set_ylim((0, 0.025))
    axs[2].set_xlim((0, 5))
    fig.tight_layout()

    fig.savefig("H_He_ORCA_analytical_orbitals_compare.pdf")

    plt.show()

"""
H 2s analytical nodes:  [1.99872715 2.00117099]
H 3s analytical nodes:  [1.90076491 1.90297906 7.09792453 7.09822852]


He+ 2s analytical nodes:  [0.99821945 1.00141557]
Ma'h                       0.9953296514296943
He+ 3s analytical nodes:  [0.9476568  0.95449141 3.54873696 3.54934585]
Ma'h                       0.8834323608145223    2.6237167003353847


H analytical energy, n:1, E:-0.49972237645752354
H analytical energy, n:2, E:-0.12493059411438089
H analytical energy, n:3, E:-0.05552470849528039
He+ analytical energy, n:1, E:-1.9988895058300942
He+ analytical energy, n:2, E:-0.49972237645752354
He+ analytical energy, n:3, E:-0.22209883398112157
"""

def plot_it_yeah(ax1, wf_3d, rx, ry, plotcount, label, NO):
    MARKERS = [".", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8", "s",
               "p", "P", "*"]
    ax1.plot(rx, ry,
             label=label,
             marker=MARKERS[NO])
    ax1.set_title("Radial density")
    ax1.set_ylim((-0.01, 0.1))
    ax1.set_xlim((-0.01, 6))
    ax1.set_xlabel("r ($a_0$)")
    ax1.legend()

if __name__ == "__main__":
    main()