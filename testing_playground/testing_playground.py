import numpy as np
import matplotlib.pyplot as plt

from ORCA_eldens import read_density_formats as formats, \
    dens_3d_grid_to_radial as radial

import main as m


# Experimenting and testing part


def main():
    test_gaussian()

    plt.show()

def figi2():
    fig = plt.figure(figsize=(9, 7), num="figi2")
    ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=1, rowspan=1)
    ax2 = plt.subplot2grid((2, 2), (0, 1), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((2, 2), (1, 0), colspan=1, rowspan=1)
    ax4 = plt.subplot2grid((2, 2), (1, 1), colspan=1, rowspan=1)

    fig.suptitle("Testing")

    return (ax1, ax2, ax3, ax4)

def test_gaussian():

    ax1, ax2, ax3, ax4 = figi2()

    #plot He CI
    if True:
        eg = formats.read_gaussian_3d_cube_grid_format()
        rx_ga, ry_ga = radial.calculate_normalized_radial_profile(
                        eg.mat, eg.voxel_length, eg.grid_size)  # axs1[1]
        #plot_3d_dens(eg.mat, ax1, ax2, ax3)
        ax4.plot(rx_ga, ry_ga, label="He_CI")

    # plot He HF
    if True:
        eg_ao0 = formats.read_gaussian_3d_cube_grid_format("He_BO.ao1.cube")
        rx_ga_ao0, ry_ga_ao0 = radial.calculate_normalized_radial_profile(
                                eg_ao0.mat**2, eg_ao0.voxel_length, eg_ao0.grid_size)
        eg_ao1 = formats.read_gaussian_3d_cube_grid_format("He_BO.ao1.cube")
        rx_ga_ao1, ry_ga_ao1 = radial.calculate_normalized_radial_profile(
                                eg_ao1.mat ** 2, eg_ao1.voxel_length, eg_ao1.grid_size)
        eg_ao2 = formats.read_gaussian_3d_cube_grid_format("He_BO.ao2.cube")
        rx_ga_ao2, ry_ga_ao2 = radial.calculate_normalized_radial_profile(
                                eg_ao2.mat**2, eg_ao2.voxel_length, eg_ao2.grid_size)
        #plot_3d_dens(eg_ao1.mat, ax1, ax2, ax3)
        ax4.plot(rx_ga_ao0, ry_ga_ao0, label="ao0")
        ax4.plot(rx_ga_ao1, ry_ga_ao1, label="ao1")
        ax4.plot(rx_ga_ao2, ry_ga_ao2, label="ao2")
        ax4.legend()


def test_radial_simple_grid(axs2):
    vl = 0.1            # voxel_length
    gs = 50            # grid size
    bl = gs*vl          # box length
    mat = np.ones((gs,gs,gs), dtype=np.int_) / bl**3

    # Notice normalization so that matrix value is not scaled by gridsize
    rx, ry = m.radial_profile_3d(mat/(vl**3), bl)
    to_corner = rx[-1]

    mat_dens = mat.sum() / (gs**3) * 1/(vl**3)  # in unit box size


    print("MAT mean density: {:.6f}, sum in simulation box:{:.6f}".format(
        mat_dens,
        mat_dens * (bl**3)))

    mat_dens_anal = mat[0, 0, 0] / (vl**3)  # in unit box size

    print("MAT ANL: mean den:{:.6f}, sum in simulation box:{:.6f}".format(
        mat_dens_anal,
        mat_dens_anal * bl**3))

    sphere_dens = np.trapz(ry * 4 * np.pi * rx ** 2, x=rx) / ((4 / 3) * np.pi * to_corner ** 3)

    print("RAD mean density: {:.6f}, sum in simulation box:{:.6f}".format(
        sphere_dens,
        sphere_dens * (bl ** 3)))

    print("Volume cube:{:.6f}, sphere:{:.6f}".format(bl*bl*bl, ((4/3)*np.pi*to_corner**3)))

    print(rx)
    print(ry)
    print(mat[0:20, 0, 0])
    #axs2[1].plot([0,1], [0.01,0.011])
    #axs2[1].plot(rx, ry*10000)
    #axs2[1].plot([rx[0], rx[-1]], [(ry*10000)[0], (ry*10000)[0]])
    #axs2[1].set_title("Simple test")

def test_some_unclear_radial_values(axs):
    """Test to plot values"""
    r, psi = m.read_some_unclear_radial_values()

    axs[0].plot(r, psi, label="psi")
    axs[0].plot(r, psi ** 2, label="dens")
    axs[0].plot(r, psi ** 2 * 4 * np.pi * r, label="dens 4*pi*r^2")
    axs[0].legend()
    axs[0].set_title("some_unclear_radial_values")

def plot_3d_dens(mat, ax1, ax2, ax3):
    ax1.imshow(np.sum(mat, 0), cmap='hot', interpolation='nearest')
    ax2.imshow(np.sum(mat, 1), cmap='hot', interpolation='nearest')
    ax3.imshow(np.sum(mat, 2), cmap='hot', interpolation='nearest')

# Probably broken as my functions have changed:

# def test_eldens_3d(axs):
#     """Test center of mass things"""
#     r_x3d, r_y3d, (mat, grid) = read_eldens3d()
#
#     print("ASDF center of mass", center_of_mass(mat))
#
#     #print(grid_size, type(grid_size), "grid_size")
#     #print(box_base_crd, "box_base_crd")
#     #print(voxel_length, "voxel_length")
#     #print("flat.shape", flat.shape)
#     print("mat.shape", mat.shape)
#
#     ax1.imshow(np.sum(mat, 0), cmap='hot', interpolation='nearest')
#     ax2.imshow(np.sum(mat, 1), cmap='hot', interpolation='nearest')
#     ax3.imshow(np.sum(mat, 2), cmap='hot', interpolation='nearest')
#
#     linestyles = ['-', '--', '-.', ':']
#
#
#     for x in [-0.5, 0, 0.5]:
#         for y in [-0.5, 0, 0.5]:
#             for z in [-0.5, 0, 0.5]:
#                 center3d = (grid / 2 - 0.5 + x, grid / 2 - 0.5 + y, grid / 2 - 0.5 + z)
#                 r_y = radial_profile_3d(mat, center3d)
#                 r_x = np.linspace(0, np.sqrt(2) * grid / 2, len(r_y))
#                 axs[2].plot(r_x, r_y, label="{} {} {}".format(x,y,z),
#                          linestyle=linestyles[int((1+x) + (1+y)*2 + (1+z)*4)%len(linestyles)])
#                 # print((x,y,z), (r_y[0:20]*np.arange(20)).sum())
#     axs[2].legend()
#
#
#     print("maximum density: ", np.unravel_index(np.argmax(mat), (grid,grid,grid)))
#     print("densities in near origin\n", mat[44:47, 44:47, 44:47])
#
#     axs[3].plot(r_x3d, r_y3d, label="density")
#     axs[3].legend()

def test_normalizations(r1_x, r1_y, eldens3d):
    norm_wf1 = (r1_y * 4 * np.pi * r1_x ** 2).sum()
    norm_dn1 = (r1_y ** 2 * 4 * np.pi * r1_x ** 2).sum()
    print("1. some_unclear_radial_values normalization [fn / fn squared]")
    print(norm_wf1, norm_dn1)
    print("")


    norm2_r = (eldens3d.r_y * 4 * np.pi * eldens3d.r_x ** 2).sum() / eldens3d.box_size.prod()
    norm2_3d = eldens3d.mat.sum()
    print("norm2_r, norm2_3d", norm2_r, norm2_3d)






main()