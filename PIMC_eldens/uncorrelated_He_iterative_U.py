import numpy as np
#import matplotlib.pyplot as plt
from typing import List, Tuple
import sys
import os
import shutil
from copy import deepcopy

# add these on your project settings. In Pycharm:
#       File - settings - Project: XXX - project interpreter -
#       gear icon on top right - show paths in selected interpreter -
#       add path /home/user/path/to/PIMC-automatization
import read_data.read_calc_data as read_calc_data
#from read_data.utilities.read_particles import particle_labels
import start_PIMC.run_set as run_set
from start_PIMC.generate_dir_names import generate_directory_names
#import plot_data.plot_energy as plot_E
import PIMC_eldens.PIMC_radial_eldens as PIMC_PC
from ORCA_eldens.generate_potential import radial_potential





help_text = """
Iterative (SCF) pimc-runner for He. Creates new calculations with external 1s 
potential. 

This starts simulation from bare sample-directrory. Correctly modified  
configuration_instance.py is still needed. For He-singlet see template:
templates/modified_configurations/configure_instance_ITERATIVE_HE_POTENTIAL.py

Other trivia:
    - For plotting, there's this other file: plot_iterations
    - Default for potential is name is hard coded: 'He_1s_rad_U'

Usage:
    # copy this file to sample-directory and run it:
    python3 uncorrelated_He_iterative

"""


def main():
    run_set.initial_error_checking()
    if os.path.isfile("../results/metadata/initial_configuration_HEAD.json"):
        print("Running iterative simulation.")
        init_setup_old = read_calc_data.read_initial_setup_from_JSON(
                            "../results/metadata/initial_configuration_HEAD.json")
    else:
        if not os.path.isfile("../results/metadata/initial_configuration.json"):
            print("Starting first normal simulation")
            run_set.start()
        print("Starting iterative simulation.")
        init_setup_old = read_calc_data.read_initial_setup_from_JSON(
                            "../results/metadata/initial_configuration.json")

    # exeeded potential convergence iteration count
    if init_setup_old.range2[0] > 100:
        print("Convergence-limit achieved. How many eternities did this calculation take?")
        return

    os.chdir("..")
    #print("READING PC FROM:", init_setup_old.dir1_labels, init_setup_old.dir2_labels)
    PCs = PIMC_PC.get_PIMC_radial_eldens(init_setup_old)
    os.chdir("sample")

    #ext = PIMC_PC.get_external_potential(name)

    init_setup_new, init_setup_old = create_new_instance(init_setup_old)
    copy_calc_folder(init_setup_old, init_setup_new)

    # Read radial electron density from PIMC pair correlation.
    r, y = read_PC_to_el_dens(PCs)

    U = radial_potential(r, y)
    write_rad_U(init_setup_new, r, y, U)
    run_set.store_setup(init_setup_new,
                        "../results/metadata/initial_configuration_HEAD.json")
    init_setup_all = generate_combined_setup_file(init_setup_new)
    run_set.store_setup(init_setup_all)

    # call PIMC iteratively
    # note that configure_instance_ITERATIVE.py has modified run_set(),
    # (it waits to end of process)
    run_set.continue_simulation(init_setup_new)
    if not init_setup_new.sbatch:
        print("Calling main iteratively!")
        main()
    else:
        print("Remember to call itself in SBATCH.sh")



def create_new_instance(init_setup_old):
    """Generates new "pointer" to most recent iteration in form of setup-file.
    """
    sys.path.append(os.getcwd())
    import configure_instance  # You must be in same folder with this py-file

    #init_setup_new = deepcopy(init_setup_old)
    init_setup_new = configure_instance.InitialSetup()

    init_setup_new.range2 = [init_setup_old.range2[0]+1]

    init_setup_new.str_range1, init_setup_new.str_range2, \
    init_setup_new.dir1_labels, init_setup_new.dir2_labels \
                                    = generate_directory_names(init_setup_new)

    return init_setup_new, init_setup_old

def copy_calc_folder(init_setup, init_setup_new):
    """Copies existing file to be new starting position to next iteration."""
    #dir1_base_name = init_setup.latex_symbol1.replace("\\", "")
    #dir1_new_label = "{}{}".format(dir1_base_name, init_setup_new.range2[0])
    fr =  "../" + init_setup.dir1_labels[0] + "/" + init_setup.dir2_labels[0]
    to = "../" + init_setup_new.dir1_labels[0] + "/" + init_setup_new.dir2_labels[0]
    shutil.copytree(fr, to)
    shutil.rmtree(to + "/" + "obs")
    os.remove(to + "/" + "pp/interaction.e1.P1.h5", )



def read_PC_to_el_dens(PCs):
    """Converts PIMC pair correlation to radial electron density."""
    # get He - e pair correlation
    rho = None
    r = None
    found = 0
    list_found = []
    for i, (x, y, lab) in enumerate(PCs):
        #if True:
        if (("He" in lab) or ("p" in lab)) and ("e" in lab) and ("P" not in lab):
            found += 1
            list_found.append(lab)
            if rho is None:
                rho = y / (4*np.pi*x**2 * np.trapz(y,x))
                r = x
            else:
                if found > 2:
                    print("list_found", list_found)
                    raise Exception("There sould be only two e–He - pairs!")
                rho_new = y / (4 * np.pi * x ** 2 * np.trapz(y, x))
                #if any(rho/rho_new - 1 > 0.5):
                #   raise Exception("Propably not e–He-potential")
                rho += rho_new
                rho /= 2
    if rho is None:
        raise Exception("Crap. He–e pair not found")
    return r, rho

def write_rad_U(init_setup_new, r, y, U):
    """Writes the radial potential to file. NOTE: MODIFY GENERATED NAMES IF
    NEEDED."""

    U_name = "He_1s_rad_U"
    rho_name = "He_1s_rad_rho"

    with open("../" + init_setup_new.dir1_labels[0] +
                    "/" + init_setup_new.dir2_labels[0] +
                    "/" + U_name, "w") as f:
        # First r-value must be zero, or external pair potential matrix squaring
        # freezes
        rho_at_0 = linear_interpolate(0.0, r[0:2], U[0:2])
        #rho_at_0 = cubic_extrapolate(0.0, r, U)
        f.write("{} {}\n".format(0.0, rho_at_0))
        for i in range(len(U)):
            f.write("{} {}\n".format(r[i], U[i]))

    with open("../" + init_setup_new.dir1_labels[0] +
                    "/" + init_setup_new.dir2_labels[0] +
                    "/" + rho_name, "w") as f:
        for i in range(len(y)):
            f.write("{} {}\n".format(r[i], y[i]))


def linear_interpolate(x, x_data, y_data):
    """
    Interpolates (or extrapolates) data linearly.
    :param x:           x-datapoint in which y is evaluated
    :param x_data:      list or array (must be ordered)
    :param y_data:      list or array
    :return:
    """
    assert len(x_data) == len(y_data)
    def ordered(l):
        return all(l[i] <= l[i + 1] for i in range(len(l) - 1))
    assert ordered(x_data)

    dydx = np.zeros(len(x_data)-1, dtype=np.float_)
    for i in range(len(x_data)-1):
        dx = x_data[i+1] - x_data[i]
        dy = y_data[i+1] - y_data[i]
        dydx[i] = dy/dx


    mean_deriv = dydx.mean()
    if x > x_data[-1]:
        closest = -1
    elif x < x_data[0]:
        closest = 0
    else:
        def find_nearest_idx(array, value):
            array = np.asarray(array)
            idx = (np.abs(array - value)).argmin()
            return idx  # array[idx]

        closest = find_nearest_idx(x_data, x)

    dx = x - x_data[closest]

    return mean_deriv * dx + y_data[closest]

def cubic_extrapolate(x, x_data, y_data):
    """
    Interpolates (or extrapolates) data using cubic spline. Warning,
    extrapolation must not be too far away from nearest data point.
    :param x:           x-datapoint in which y is evaluated
    :param x_data:      list or array (must be ordered)
    :param y_data:      list or array
    :return:
    """
    from scipy.interpolate import InterpolatedUnivariateSpline as Spline

    spl = Spline(x_data, y_data)
    return spl(x)


def generate_combined_setup_file(init_setup_new):
    """Update written metadata so that 'python3 -m plot_data.main' -command plots
    all iterations in same picture (instead of the most recent one). Overwrites
    initial_configuration.json.
    """
    if init_setup_new.range2[0] == 1:
        shutil.copy("../results/metadata/initial_configuration.json",
                    "../results/metadata/initial_configuration_ITR0.json")

    init_setup_all = deepcopy(init_setup_new)
    init_setup_all.range2 = list(range(init_setup_new.range2[0]))

    init_setup_all.str_range1, init_setup_all.str_range2, \
    init_setup_all.dir1_labels, init_setup_all.dir2_labels \
                                    = generate_directory_names(init_setup_all)

    return init_setup_all


if __name__ == "__main__":
    tst = False
    if tst:
        os.chdir("/home/alpi/kesa_2018/laskut/laskut_NOSYNC/meriharakka/v9/He_no_corr_half/sample/")
        main()
        exit(0)
    # if len(sys.argv) != 2:
    #     print(help_text)
    #     raise ValueError("Missing argument: Give name of external potential file for PIMC.")
    main()
