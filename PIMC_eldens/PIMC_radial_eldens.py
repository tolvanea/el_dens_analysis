import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
from typing import List, Tuple
import sys
import os


# add these on your project settings. In Pycharm:
#       File - settings - Project: XXX - project interpreter -
#       gear icon on top right - show paths in selected interpreter -
#       add path /home/user/path/to/PIMC-automatization
import read_data.read_calc_data as read_calc_data
from read_data.utilities.read_particles import particle_labels
#import plot_data.plot_energy as plot_E
import plot_data.plot_paircorrelation as plot_PC




help_text = """
Read pimc paircorrelation and plot it as radial e-density. Do not give potential 
as parameter ("He_1s_rad_U"), give the radial electron density ("He_1s_rad_rho")!

This code is quite ugly modification from my existing code. (There's excess 
functionality that is not used. But hey, who cares?)

Usage:
    python3 -m PIMC_eldens.PIMC_radial_eldens PP-file  # i.e. 'He_1s_rad_rho'

"""


def main(name):
    fig = plt.figure(figsize=(5, 4), num="figi{}".format(np.random.randint(1000)))
    ax1 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)

    PCs = call_PIMC_eldens(None)
    ext = get_external_potential(name)
    plot_both(PCs, ext, fig, ax1)

    plt.show()

def call_PIMC_eldens(ax1=None):
    cwd = os.getcwd()  # not a chewing gum hack or anything

    if not os.path.isdir("../../sample"):
        raise Exception("Run this from single calulation directory. " +
                        "Generate calculations with PIMC-automatization.")
    os.chdir("../..")
    init_setup = read_calc_data.read_initial_setup_from_JSON()
    PCs = get_PIMC_radial_eldens(init_setup, ax1)

    os.chdir(cwd)

    return PCs

def get_PIMC_radial_eldens(init_setup, ax=None):
    """Get a pair correlation distribution. Plot if axes given."""

    # generate particle pairs like: ["PC/e_p/", "PC/p_p/", "PC/e_e/"]
    prs, _ = particle_labels()
    pairs = []
    titles = ["", "", "", ""]
    for i in range(len(prs)):
        for j in range(len(prs)):
            pairs.append("".join(["PC/", prs[i], "_", prs[j], "/"]))

    return_PCs = []

    #axs = [axes[0][0], axes[0][1], axes[1][0], axes[1][1]]
    axs = [ax]
    for i, pair in enumerate(pairs):
        i_mod = np.mod(i, 1)  # lazy ass I am using old code. (What is modulus one?)
        ax = axs[i_mod]
        try:
            data, attrs = read_calc_data.read_all_observables(
                init_setup,
                pair + "PC",
                filename="paircorrelation.h5")
            succ = attrs["succeed"]

            r_grid, rd = read_calc_data.read_all_observables(
                init_setup,
                pair + "r",
                filename="paircorrelation.h5")

            plot_pair_correlation(ax, data, attrs, succ, r_grid, pair, init_setup, return_PCs)
            titles[i_mod] += " " + pair
            if ax is not None:
                ax.set_title(titles[i_mod])


        except ValueError:  # pair does not exist
            pass
            #ax.set_title(pair + " not found")

    return return_PCs

def plot_pair_correlation(ax, data, attrs, succ, r_grid, pair, init_setup, return_PCs):
    """This function was designed to be PC plotter, however, it's now PC getter.
    """
    nz = np.nonzero(succ)
    for i in range(len(nz[1])):
        idx = (nz[0][i], nz[1][i])  # index of succesfull simulation


        PC, PC_err = plot_PC.PC_sem(data, attrs, idx)
        r = r_grid[idx][:]

        tau_is ="${}={}{}$".format(init_setup.latex_symbol1,
                                  init_setup.str_range1[idx[0]],
                                  init_setup.latex_unit1)
        R_is = "${}={}{}$".format(init_setup.latex_symbol2,
                                 init_setup.str_range2[idx[1]],
                                 init_setup.latex_unit2)

        label = ", ".join([pair[3:-1], R_is])

        return_PCs.append((r, PC, pair[3:-1]))

        if ax is not None:
            ax.errorbar(r, PC/(4*np.pi*r**2), PC_err, label=label)
            ax.legend()


def get_external_potential(name):
    A = np.genfromtxt(name, delimiter=' ')

    return (A[:, 0], A[:, 1], "external_Ev")


def plot_both(PCs, ext, fig, ax1):
    for i, (x, y, lab) in enumerate(PCs):
        #if True:
        if ("He" in lab) or ("p" in lab):
            #print("PC integral should be one", np.trapz(y,x))
            #print("sum should not be one", np.sum(y))
            rho = y / (4*np.pi*x**2 * np.trapz(y,x))
            ax1.plot(x, rho, label="PIMC el dens, " + lab)

    (xd, yd, labd) = ext
    #print("density integral should be one", np.trapz(yd*4*np.pi*xd**2, xd))
    ax1.plot(xd, yd, label="ORCA 1s")  # labd
    ax1.legend()
    ax1.set_xlabel("r ($a_0$)")
    ax1.set_ylabel("$\\rho$")
    ax1.set_title("Radial electron density")
    ax1.set_yscale("log", nonposy='clip')
    fig.tight_layout()
    fig.savefig("PIMC_ORCA_el_density.pdf")






if __name__ == "__main__":
    tst = False
    if tst:
        os.chdir("/home/alpi/kesa_2018/laskut/laskut_NOSYNC/csc/v8/He_tarkka_kevyempi/T10000.0/tau0.005/")
        main("He_1s_rad_rho")
        exit(0)

    if len(sys.argv) != 2:
        print(help_text)
        raise ValueError("Missing argument: Give name of external potential file for PIMC.")
    main(sys.argv[1])
