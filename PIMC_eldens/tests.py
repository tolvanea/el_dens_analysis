import unittest
import numpy as np
import PIMC_eldens.uncorrelated_He_iterative_U as U
import matplotlib.pyplot as plt


class TestStuff(unittest.TestCase):

    def test_linear_interpolate(self):
        x = [0.1, 0.2]
        y = [0.2, 0.1]
        intrp = U.linear_interpolate(0.0, x, y)
        print("intrp", intrp)
        self.assertTrue(np.abs(intrp - 0.3) < 0.001)

        x = [0.1, 0.2, 0.3]
        y = [0.1, 0.2, 0.3]
        intrp = U.linear_interpolate(0.0, x, y)
        print ("intrp", intrp)
        self.assertTrue(np.abs(intrp - 0) < 0.001)

        x = [0.1, 0.2, 0.3, 0.4]
        y = list(reversed([0.1, 0.2, 0.3, 0.4]))
        intrp = U.linear_interpolate(0.5, x, y)
        print("intrp", intrp)
        self.assertTrue(np.abs(intrp - 0.0) < 0.001)

    #def test_plot_em(self):
    def plot_em(self):
        A = np.genfromtxt('He_1s_rad_U', delimiter=' ')
        plt.plot(A[:, 0], A[:, 1])
        lin = U.linear_interpolate(0, A[:2, 0], A[:2, 1])
        cub = U.cubic_extrapolate(0, A[:, 0], A[:, 1])
        plt.plot([0], [lin], marker="*", label="linear")
        plt.plot([0], [cub], marker="o", label="cubic")

        for i in np.linspace(-1, 0, 10):
            cub = U.cubic_extrapolate(i, A[:, 0], A[:, 1])
            plt.plot([i], [cub], marker=".", label="cubic")
        plt.show()

    def test_cubic_extrapolate(self):
        A = np.genfromtxt('He_1s_rad_U', delimiter=' ')
        plt.plot(A[:, 0], A[:, 1])
        lin = U.linear_interpolate(0, A[:2, 0], A[:2, 1])
        cub = U.cubic_extrapolate(0, A[:, 0], A[:, 1])
        self.assertTrue(A[0, 1] < cub and cub < lin)


if __name__ == '__main__':
    unittest.main()
