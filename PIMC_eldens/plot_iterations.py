import os
import numpy as np
import matplotlib.pyplot as plt
from os import path, getcwd

import read_data.read_calc_data as read_calc_data

CONVERGED_NUM = 6  # fix-by-hand number that plots unconverged lines differently

def plot_radial_densities(init_setup):
    fig1 = plt.figure(figsize=(5,4), num="figi{}".format(123))
    ax1 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)

    for i, dir1 in enumerate(init_setup.dir1_labels):
        for j, dir2 in enumerate(init_setup.dir2_labels):
            if j==0:
                continue
            path0 = os.getcwd() + "/" + dir1 + "/" + dir2

            A = np.genfromtxt(path0 + '/He_1s_rad_rho', delimiter=' ')
            B = np.genfromtxt(path0 + '/He_1s_rad_rho_ORIG', delimiter=' ')

            basename = path.basename(path0)
            line = "-" if j > CONVERGED_NUM else ":"
            linew = 1.0 if j > CONVERGED_NUM else 0.8
            ax1.plot(A[:, 0], A[:, 1], label="PIMC " + basename,
                     ls=line, lw=linew)
            if j==len(init_setup.dir2_labels)-1:
                ax1.plot(B[:, 0], B[:, 1], "k", label="ORCA 1s", linestyle="--",
                         linewidth=1.5)
            #print("Whats the matter?", dir1, dir2)


    ax1.set_xlabel("r $(a_0)$")
    ax1.set_ylabel("$\\rho$")

    ax1.legend(bbox_to_anchor=(1.04,1), loc="upper left")
    ax1.set_xlim((-0.04, 1.04))
    #fig1.tight_layout()
    fig1.savefig("results/iteration_densities_linear.pdf")

    ax1.set_ylim((1e-13, 4))
    ax1.set_yscale("log", nonposy="clip")
    ax1.set_xlim((-0.4, 10.4))
    #fig1.tight_layout()
    #ax1.set_title("Electron density")
    fig1.savefig("results/iteration_densities_log.pdf")


def plot_radial_density_fractions(init_setup):
    fig1 = plt.figure(figsize=(5,4), num="figi{}".format(1234))
    ax1 = plt.subplot2grid((1, 1), (0, 0), colspan=1, rowspan=1)

    from scipy.interpolate import UnivariateSpline


    for i, dir1 in enumerate(init_setup.dir1_labels):
        for j, dir2 in enumerate(init_setup.dir2_labels):
            path0 = os.getcwd() + "/" + dir1 + "/" + dir2

            A = np.genfromtxt(path0 + '/He_1s_rad_rho', delimiter=' ')
            B = np.genfromtxt(path0 + '/He_1s_rad_rho_ORIG', delimiter=' ')

            A_spl = UnivariateSpline(A[:, 0], A[:, 1], s=0)
            B_spl = UnivariateSpline(B[:, 0], B[:, 1], s=0)  # quick fix B[:, 0]*0.99 + 0.01

            x = np.linspace(0, A[-1, 0], A.shape[0])

            if j==0:
                continue

            basename = path.basename(path0)
            line = "-" if j > CONVERGED_NUM else ":"
            linew = 1.0 if j > CONVERGED_NUM else 0.8
            ax1.plot(x, A_spl(x)/B_spl(x), label="PIMC " + basename + "",
                     ls=line, lw=linew)
            if j == len(init_setup.dir2_labels) - 1:
                ax1.plot([0, x[-1]], [1, 1], "k", label="y=1", linestyle="--",
                         linewidth=1.5)
            #ax1.plot(B[:, 0], B[:, 1], label="ORCA 1s")
            #print("Whats the matter?", dir1, dir2)

    #ax1.set_yscale("log", nonposy="clip")
    ax1.legend(bbox_to_anchor=(1.04,1))
    ax1.set_xlabel("r $(a_0)$")
    ax1.set_ylabel("ratio:  PIMC / ORCA")
    #ax1.set_title("Proportional to ORCA 1s")
    ax1.set_ylim((-0.05, 1.6))
    ax1.set_xlim((-0.4, 10.4))
    #fig1.tight_layout()
    fig1.savefig("results/iteration_density_errors.pdf")





if __name__ == "__main__":
    init_setup = read_calc_data.read_initial_setup_from_JSON(
        "results/metadata/initial_configuration.json")

    plot_radial_densities(init_setup)
    plot_radial_density_fractions(init_setup)

    plt.show()